/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smarthash',
  version: '3.0.2',
  description: 'simplified access to node hash functions'
}
